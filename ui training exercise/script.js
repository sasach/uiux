document.querySelector('#error').style.display = 'none';
function passwordMatch() {
    var p1 = document.getElementById('password').value;
    var p2 = document.getElementById('cpassword').value;
    if(!(p1==p2) && p1 != '' && p2 != '')
        document.querySelector('#error').style.display = 'inline';
    else
        document.querySelector('#error').style.display = 'none';
}
function submit() {
    var inputs = document.querySelectorAll('input');
    var result = {};
    for(var i = 0; i < inputs.length; i++){
        if(!(inputs[i].value)) {
            alert('All fields are mandatory.')
            break;
        }
        if(inputs[i].type == 'radio')
            result[inputs[i].id] = inputs[i].checked;
        else
            result[inputs[i].id] = inputs[i].value;
    }
    if(i == inputs.length)
        console.log(result);
}
function cancel(){
    var inputs = document.querySelectorAll('input');
    for(var i = 0; i < inputs.length; i++){
        if(inputs[i].type == 'radio')
            inputs[i].checked = false;
        else
            inputs[i].value = '';
    }
}